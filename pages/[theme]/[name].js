import React from 'react'
import { isMobile } from 'react-device-detect';
import Head from 'next/head'
import Layout from '../../components/layout'
import '../../style.css'
import Sharing from '../../components/sharing'

var theme_colors = {
  '01': '#CA3839',
  '02': '#FCB14D',
  '03': '#FEB750',
  '04': '#E72513',
  '05': '#EA5048',
  '06': '#EDE7D7',
  '07': '#D0200E'
}

var text_colors = {
  '01': '#FBC262',
  '02': '#000000',
  '03': '#000000',
  '04': '#FFFFFF',
  '05': '#000000',
  '06': '#000000',
  '07': '#000000'
}

const GA_TRACKING_ID = 'UA-131764982-2';

class Index extends React.Component {
  constructor (props) {
    super(props)
    this.state = props.card;
  }

  handleChange = ({ theme, name }) => {
    this.setState({ 
      theme: theme,
      name: name
    })
  }

  render () {
    if (isMobile) {
      theme_colors['04'] = '#E60012'
    }

    const theme_style = {
      backgroundColor: theme_colors[this.state.theme || '01']
    };

    const text_style = {
      color: text_colors[this.state.theme || '01']
    }

    return (
      <Layout>
        <Head>
          <title>{ `${this.state.name} 送來一份祝賀~` }</title>

          <meta property='og:title' content={ `${this.state.name} 送來一份祝賀~` } />
          <meta property="og:site_name" content={ `${this.state.name} 送來一份祝賀~` }></meta>
          <meta property="og:url" content="https://newyear.mosan.com.tw/"></meta>
          <meta property="og:image" content={ `https://newyear.mosan.com.tw/card-${this.state.theme || '01'}.jpg` }></meta>
          <meta property="og:description" content="請點連結開啟，輸入您的名字還可以把專屬敬賀分享給親友"></meta>
          <meta name='description' content="請點連結開啟，輸入您的名字還可以把專屬敬賀分享給親友" />
          <meta name="robots" content="noindex,nofollow"></meta>
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta charSet='utf-8' />

          <link rel="icon" href="https://newyear.mosan.com.tw/logo.jpg" sizes="192x192"/>

          <script async src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`} />
          <script
            dangerouslySetInnerHTML={{
              __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments)};
                gtag('js', new Date());
                gtag('config', '${GA_TRACKING_ID}');
              `,
            }}
          />
        </Head>

        <div>
          <article className="card">
            <div className="row card-image center">
              <img src={`/card-${this.state.theme || '01'}.jpg`} alt=""/>
            </div>
            <div className="row card-body" style={theme_style}>
              <p style={text_style}><span>{this.state.name}</span> 敬賀</p>
            </div>
          </article>
  
          <Sharing name={this.state.name} theme={this.state.theme} onChange={this.handleChange} />
        </div>
  
        <style jsx>{`
          .card-body {
            background-color: #CA3839;
          }
          img {
            width: 100%;
            height: 100%;
            display: block;
          }
          p {
            width: 100%;
            padding: 0 20px;
            text-align: right;
            font-weight: 500;

            color: #000;
            font-size: 2.2rem;
          }
          p span {
            margin-right: 10px;
          }
          @media screen and (max-width: 992px) {
            p span {
              margin-right: 10px;
            }
            p {
              font-size: 1.5rem;
            }
          }
        `}</style>
      </Layout>
    )
  }
}

Index.getInitialProps = ({ query, req }) => {
  return {
    card: {
      name: query.name,
      theme: query.theme,
      hostname: req.headers.host
    }
  }
}

export default Index;
import React from 'react';
import { isMobile } from 'react-device-detect';

const shareLinkBase = {
  true: "line://msg/text/?https://newyear.mosan.com.tw",
  false: "https://social-plugins.line.me/lineit/share?text=https://newyear.mosan.com.tw"
}

class Sharing extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: this.props.name,
      shareLink: `${shareLinkBase[isMobile]}/${this.props.theme}/${this.props.name}`
    }
  }

  handleChange = data => {
    this.props.onChange(data)

    this.setState({
      shareLink: `${shareLinkBase[isMobile]}/${data.theme}/${data.name}`
    })
  }

  handleNameChange = e => {
    this.handleChange({
      theme: this.props.theme,
      name: e.target.value
    })
  }

  handleThemeChange = e => {
    this.handleChange({
      theme: e.target.value,
      name: this.props.name
    })
  }

  render () {
    return (
      <div className="sharing">
        <div className="row share-inputs center">
          <input type="text" placeholder="請輸入您的姓名分享給朋友" onChange={this.handleNameChange} />
  
          <select name="template" defaultValue="" onChange={this.handleThemeChange}>
            <option value="" disabled hidden>選擇賀卡 ▼</option>
          　<option value="01">1.鼠年迎新春</option>
          　<option value="02">2.經典迎春納福</option>
          　<option value="03">3.大氣花開富貴</option>
          　<option value="04">4.簡約謹賀新年</option>
          　<option value="05">5.日式謹賀新年</option>
          　<option value="06">6.現代福壽雙全</option>
          　<option value="07">7.傳統年年有餘</option>
          </select> 
        </div>
  
        <div className="row center">
          <a href={ this.state.shareLink }>
            <img src={ "/line2.png" } alt="用LINE傳送分享給好友" />
            用LINE傳送分享給好友
          </a>
        </div>
  
        <style jsx>{`
          .sharing {
            margin-left: 10px;
            margin-right: 10px;
            margin-top: 30px;
            margin-bottom: 35px;
          }
          a {
            display: block;
            color: #fff;
            text-decoration: none;
            text-align: center;
  
            background-color: #24BB66; 
  
            border-radius: 10px;
            width: 100%;
            margin-top: 15px;
            margin-bottom: 5px;
          }
          input, select {
            padding: .375rem .75rem .375rem .75rem;
            background-color: #fff;
            font-size: 1.1rem;
            margin-bottom: 5px;
          }
          input {
            height: 36px;
            border: 1px solid #EF3B73;
            min-width: 200px;
            color: #ABABAB;
            border-radius: 10px 0 0 10px;
  
            width: calc(100% - 150px);
          }
          select {
            border: 1px #EF3B73 solid;
            background-color: #EF3B73;
            color: #fff;
            height: 50px;
            position: relative;
            appearance: none;
            outline: none;
            border-radius: 0 10px 10px 0;
  
            width: 150px;
          }
          option {
            font-size: 1.1rem;
            padding: 0px 2px 1px;
          }
          ::placeholder {
            font-size: 1rem;
            color: #ABABAB;
          }
          a span {
            font-size: 1.2rem;
          }
          a img {
            vertical-align: middle;
            width: 40px;
          }
        `}</style>
      </div>
    )
  }
}

export default Sharing

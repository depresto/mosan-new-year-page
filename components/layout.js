import React from 'react'
import Footer from './footer'

export default ({ children }) => (
  <div>
    <div className="container">
      { children }
      <Footer></Footer>
    </div>
  </div>
)

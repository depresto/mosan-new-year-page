export default () => {
  return (
    <footer>
      <div className="row">
        <a href="https://lin.ee/ciQiboP" target="_blank">
          <img src="/footer-04.gif" alt="2020掌握品牌趨勢，贏得行銷趨勢 預約您的品牌顧問師 Mosan 摩盛整合行銷" />
        </a>
      </div>
      <div className="row">
        <a href="https://mosan.com.tw/" target="_blank">
          <img src="/footer-03.png" alt="Mosan 摩盛整合行銷 祝您新年快樂！" />
        </a>
      </div>

      <style jsx>{`
        a {
          display: block;
          width: 100%;
        }
        img {
          width: 100%;
        }
      `}</style>
    </footer>
  )
}
